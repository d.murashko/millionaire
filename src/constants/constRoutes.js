export const routes = {
    home: {name: 'main', href: '/'},
    game: {name: 'Game', href: '/game'},
    result: {name: 'score', href: '/result'}
};